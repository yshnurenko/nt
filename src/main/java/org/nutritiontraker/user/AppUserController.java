package org.nutritiontraker.user;

import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;


@RestController
@RequestMapping("/users")
public class AppUserController {

    private final AppUserService appUserService;
    private final ModelMapper modelMapper;

    public AppUserController(AppUserService appUserService, ModelMapper modelMapper) {
        this.appUserService = appUserService;
        this.modelMapper = modelMapper;
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public AppUserDto createAppUser(@RequestBody AppUserDto appUserDto) {
        AppUser appUser = modelMapper.map(appUserDto, AppUser.class);
        AppUser appUserCreated = appUserService.createAppUser(appUser);
        return modelMapper.map(appUserCreated, AppUserDto.class);
    }
}