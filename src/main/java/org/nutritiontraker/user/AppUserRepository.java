package org.nutritiontraker.user;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.Objects;

@Repository
public class AppUserRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    public AppUserRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public AppUser saveAppUser(AppUser appUser) {
        String sql = "INSERT INTO app_user (name) VALUES (:name) RETURNING id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("name", appUser.getName());

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(sql, params, keyHolder);

        int generatedId = Objects.requireNonNull(keyHolder.getKey()).intValue();

        appUser.setId(generatedId);

        return appUser;
    }
}
