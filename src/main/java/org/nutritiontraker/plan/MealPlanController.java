package org.nutritiontraker.plan;

import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/meal-plans")
public class MealPlanController {

    private final ModelMapper modelMapper;
    private final MealPlanService mealPlanService;

    public MealPlanController(ModelMapper modelMapper, MealPlanService mealPlanService) {
        this.modelMapper = modelMapper;
        this.mealPlanService = mealPlanService;
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public MealPlanDto createMealPlan(@RequestBody MealPlanDto mealPlanDto) {
        MealPlan mealPlan = modelMapper.map(mealPlanDto, MealPlan.class);
        MealPlan mealPlanCreated = mealPlanService.createMealPlan(mealPlan);
        return modelMapper.map(mealPlanCreated, MealPlanDto.class);
    }

    @PutMapping(value = "/{id}", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public MealPlanDto updateMealPlan(@RequestBody MealPlanDto mealPlanDto, @PathVariable Integer id) {
        MealPlan mealPlan = modelMapper.map(mealPlanDto, MealPlan.class);
        MealPlan mealPlanUpdated = mealPlanService.updateMealPlan(mealPlan, id);
        return modelMapper.map(mealPlanUpdated, MealPlanDto.class);
    }

    @DeleteMapping("/{id}")
    public void deleteMealPlan(@PathVariable Integer id) {
        mealPlanService.deleteMealPlan(id);
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public MealPlanDto getMealPlanByDate(@RequestParam Date date) {
        MealPlan mealPlan = mealPlanService.getMealPlanByDate(date);
        return modelMapper.map(mealPlan, MealPlanDto.class);
    }
}
