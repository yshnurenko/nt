package org.nutritiontraker.plan;

public class MealPlan {

    private int id;
    private String name;
    private double appUserId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAppUserId() {
        return appUserId;
    }

    public void setAppUserId(double userId) {
        this.appUserId = userId;
    }
}