package org.nutritiontraker.plan;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class MealPlanRowMapper implements RowMapper<MealPlan> {

    @Override
    public MealPlan mapRow(ResultSet rs, int rowNum) throws SQLException {
        MealPlan mealPlan = new MealPlan();
        mealPlan.setId(rs.getInt("id"));
        mealPlan.setName(rs.getString("name"));
        mealPlan.setAppUserId(rs.getInt("app_user_id"));
        return mealPlan;
    }
}