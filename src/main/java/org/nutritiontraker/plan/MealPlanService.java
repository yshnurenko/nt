package org.nutritiontraker.plan;

import org.springframework.stereotype.Service;

import java.sql.Date;

@Service
public class MealPlanService {

    private final MealPlanRepository mealPlanRepository;

    public MealPlanService(MealPlanRepository mealPlanRepository) {
        this.mealPlanRepository = mealPlanRepository;
    }

    public MealPlan createMealPlan(MealPlan mealPlan) {
        return mealPlanRepository.saveMealPlan(mealPlan);
    }

    public MealPlan updateMealPlan(MealPlan mealPlan, Integer mealPlanId) {
        return mealPlanRepository.updateMealPlan(mealPlan, mealPlanId);
    }

    public void deleteMealPlan(Integer mealPlanId) {
        mealPlanRepository.deleteMealPlanById(mealPlanId);
    }

    public MealPlan getMealPlanByDate(Date date) {
        return mealPlanRepository.getMealPlanByDate(date);
    }
}
