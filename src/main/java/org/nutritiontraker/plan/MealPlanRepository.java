package org.nutritiontraker.plan;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.Objects;

@Repository
public class MealPlanRepository {

    private final MealPlanRowMapper mealPlanRowMapper;
    private final NamedParameterJdbcTemplate jdbcTemplate;

    public MealPlanRepository(MealPlanRowMapper mealPlanRowMapper, NamedParameterJdbcTemplate jdbcTemplate) {
        this.mealPlanRowMapper = mealPlanRowMapper;
        this.jdbcTemplate = jdbcTemplate;
    }

    public MealPlan saveMealPlan(MealPlan mealPlan) {
        String sql = "INSERT INTO meal_plan (name, app_user_id) VALUES (:name, :app_user_id) RETURNING id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("name", mealPlan.getName());
        params.addValue("app_user_id", mealPlan.getAppUserId());

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(sql, params, keyHolder);

        int generatedId = Objects.requireNonNull(keyHolder.getKey()).intValue();

        mealPlan.setId(generatedId);

        return mealPlan;
    }


    public MealPlan updateMealPlan(MealPlan mealPlan, Integer mealPlanId) {
        String sql = "UPDATE meal_plan SET name = :name, app_user_id = :app_user_id WHERE id = :id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", mealPlanId);
        params.addValue("name", mealPlan.getName());
        params.addValue("app_user_id", mealPlan.getAppUserId());

        jdbcTemplate.update(sql, params);

        return mealPlan;
    }

    public void deleteMealPlanById(Integer mealPlanId) {
        String sql = "DELETE FROM meal_plan WHERE id = :id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", mealPlanId);

        jdbcTemplate.update(sql, params);
    }

    public MealPlan getMealPlanByDate(Date date) {
        String sql = "SELECT m.id, m.name, m.app_user_id FROM meal_plan m " +
                "LEFT JOIN schedule s ON m.id = s.meal_plan_id " +
                "WHERE s.date = :date";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("date", date);

        return jdbcTemplate.queryForObject(sql, params, mealPlanRowMapper);
    }
}