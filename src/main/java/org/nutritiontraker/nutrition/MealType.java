package org.nutritiontraker.nutrition;

public enum MealType {
    BREAKFAST, LUNCH, DINNER, SNACK
}
