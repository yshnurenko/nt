package org.nutritiontraker.nutrition;

import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/nutrition")
public class NutritionController {

    private final ModelMapper modelMapper;
    private final NutritionService nutritionService;

    public NutritionController(ModelMapper modelMapper, NutritionService nutritionService) {
        this.modelMapper = modelMapper;
        this.nutritionService = nutritionService;
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public NutritionDto createNutrition(@RequestBody NutritionDto nutritionDto) {
        Nutrition nutrition = modelMapper.map(nutritionDto, Nutrition.class);
        Nutrition nutritionCreated = nutritionService.createNutrition(nutrition);
        return modelMapper.map(nutritionCreated, NutritionDto.class);
    }

    @PutMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public NutritionDto updateNutritionConsumptionByDate(@RequestBody NutritionDto nutritionDto, @RequestParam Date date) {
        Nutrition nutrition = modelMapper.map(nutritionDto, Nutrition.class);
        Nutrition nutritionUpdated = nutritionService.updateNutritionConsumptionByDate(nutrition, date);
        return modelMapper.map(nutritionUpdated, NutritionDto.class);
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public NutritionInformationDto getNutritionInformationByDate(Date date) {
        NutritionInformation nutritionInformation = nutritionService.getNutritionInformationByDate(date);
        return modelMapper.map(nutritionInformation, NutritionInformationDto.class);
    }
}
