package org.nutritiontraker.nutrition;

import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;

@Service
public class NutritionService {

    private final NutritionRepository nutritionRepository;

    public NutritionService(NutritionRepository nutritionRepository) {
        this.nutritionRepository = nutritionRepository;
    }

    public Nutrition createNutrition(Nutrition nutrition) {
        return nutritionRepository.saveNutrition(nutrition);
    }

    public Nutrition updateNutritionConsumptionByDate(Nutrition nutrition, Date date) {
        return nutritionRepository.updateNutritionConsumptionByDate(nutrition, date);
    }

    public NutritionInformation getNutritionInformationByDate(Date date) {
        List<Nutrition> nutritionList = nutritionRepository.getNutritionByDate(date);
        double T = 0;
        double C = 0;
        for (Nutrition nutrition : nutritionList) {
            if (nutrition.isWorkout() && nutrition.getMealType().name().equals("SNACK")) {
                T += nutrition.getTarget();
                C += nutrition.getConsumed();
            }
        }
        double D = Math.abs(T - C) / T;
        double score = Math.round(5 * Math.exp(-2.3 * D));

        NutritionInformation nutritionInformation = new NutritionInformation();
        nutritionInformation.setNutritionList(nutritionList);
        nutritionInformation.setScore(score);

        return nutritionInformation;
    }
}