package org.nutritiontraker.nutrition;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Repository
public class NutritionRepository {

    private final NutritionRowMapper nutritionRowMapper;
    private final NamedParameterJdbcTemplate jdbcTemplate;

    public NutritionRepository(NutritionRowMapper nutritionRowMapper, NamedParameterJdbcTemplate jdbcTemplate) {
        this.nutritionRowMapper = nutritionRowMapper;
        this.jdbcTemplate = jdbcTemplate;
    }

    public Nutrition saveNutrition(Nutrition nutrition) {
        String sql = "INSERT INTO nutrition (meal_type, nutrition_type, target, consumed, meal_plan_id) " +
                "VALUES (:meal_type, :nutrition_type, :target, :consumed, :meal_plan_id) RETURNING id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("meal_type", nutrition.getMealType().name());
        params.addValue("nutrition_type", nutrition.getNutritionType().name());
        params.addValue("target", nutrition.getTarget());
        params.addValue("consumed", nutrition.getConsumed());
        params.addValue("meal_plan_id", nutrition.getMealPlanId());

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(sql, params, keyHolder);

        int generatedId = Objects.requireNonNull(keyHolder.getKey()).intValue();

        nutrition.setId(generatedId);

        return nutrition;
    }

    public Nutrition updateNutritionConsumptionByDate(Nutrition nutrition, Date date) {
        String select = "SELECT n.id, meal_type, nutrition_type, target, consumed, n.meal_plan_id, s.is_workout_day " +
                "FROM nutrition n " +
                "INNER JOIN meal_plan m ON m.id = n.meal_plan_id " +
                "INNER JOIN schedule s ON m.id = s.meal_plan_id " +
                "WHERE s.date = :date AND meal_type = :meal_type AND nutrition_type = :nutrition_type";

        MapSqlParameterSource paramsSelect = new MapSqlParameterSource();
        paramsSelect.addValue("date", date);
        paramsSelect.addValue("meal_type", nutrition.getMealType().name());
        paramsSelect.addValue("nutrition_type", nutrition.getNutritionType().name());

        Nutrition selectNutrition = jdbcTemplate.queryForObject(select, paramsSelect, nutritionRowMapper);
        if (selectNutrition == null) {
            throw new NullPointerException("This object is null");
        }

        String update = "UPDATE nutrition SET consumed = :consumed WHERE id = :id";

        MapSqlParameterSource paramsUpdate = new MapSqlParameterSource();
        paramsUpdate.addValue("consumed", nutrition.getConsumed());
        paramsUpdate.addValue("id", selectNutrition.getId());
        jdbcTemplate.update(update, paramsUpdate);

        return nutrition;
    }

    public List<Nutrition> getNutritionByDate(Date date) {
        String sql = "SELECT n.id, meal_type, nutrition_type, target, consumed, n.meal_plan_id, s.is_workout_day " +
                "FROM nutrition n " +
                "INNER JOIN meal_plan m ON m.id = n.meal_plan_id " +
                "INNER JOIN schedule s ON m.id = s.meal_plan_id " +
                "WHERE s.date = :date";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("date", date);

        return jdbcTemplate.query(sql, params, nutritionRowMapper);
    }
}