package org.nutritiontraker.nutrition;

public enum NutritionTypeDto {
    CARBOHYDRATES, FATS, PROTEINS, SNACK
}
