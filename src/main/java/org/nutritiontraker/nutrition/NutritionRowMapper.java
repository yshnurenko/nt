package org.nutritiontraker.nutrition;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class NutritionRowMapper implements RowMapper<Nutrition> {

    @Override
    public Nutrition mapRow(ResultSet rs, int rowNum) throws SQLException {
        Nutrition nutrition = new Nutrition();
        nutrition.setId(rs.getInt("id"));
        nutrition.setMealType(MealType.valueOf(rs.getString("meal_type")));
        nutrition.setNutritionType(NutritionType.valueOf(rs.getString("nutrition_type")));
        nutrition.setTarget(rs.getDouble("target"));
        nutrition.setConsumed(rs.getDouble("consumed"));
        nutrition.setMealPlanId(rs.getInt("meal_plan_id"));
        nutrition.setWorkout(rs.getBoolean("is_workout_day"));
        return nutrition;
    }
}