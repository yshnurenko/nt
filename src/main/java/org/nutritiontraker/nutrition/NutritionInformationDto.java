package org.nutritiontraker.nutrition;

import java.util.List;

public class NutritionInformationDto {

    private List<Nutrition> nutritionList;
    private double score;

    public List<Nutrition> getNutritionList() {
        return nutritionList;
    }

    public void setNutritionList(List<Nutrition> nutritionList) {
        this.nutritionList = nutritionList;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }
}
