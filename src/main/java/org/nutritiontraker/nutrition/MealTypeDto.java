package org.nutritiontraker.nutrition;

public enum MealTypeDto {
    BREAKFAST, LUNCH, DINNER, SNACK
}
