package org.nutritiontraker.schedule;

import java.sql.Date;

public class ScheduleDto {

    private int id;
    private String name;
    private Date date;
    private boolean isWorkoutDay;
    private int mealPlanId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isWorkoutDay() {
        return isWorkoutDay;
    }

    public void setWorkoutDay(boolean workoutDay) {
        isWorkoutDay = workoutDay;
    }

    public int getMealPlanId() {
        return mealPlanId;
    }

    public void setMealPlanId(int mealPlanId) {
        this.mealPlanId = mealPlanId;
    }
}