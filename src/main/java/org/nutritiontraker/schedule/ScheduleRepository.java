package org.nutritiontraker.schedule;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.Objects;

@Repository
public class ScheduleRepository {

    private final ScheduleRowMapper scheduleRowMapper;
    private final NamedParameterJdbcTemplate jdbcTemplate;


    public ScheduleRepository(ScheduleRowMapper scheduleRowMapper, NamedParameterJdbcTemplate jdbcTemplate) {
        this.scheduleRowMapper = scheduleRowMapper;
        this.jdbcTemplate = jdbcTemplate;
    }

    public Schedule saveSchedule(Schedule schedule) {
        String sql = "INSERT INTO schedule (name, date, is_workout_day, meal_plan_id) " +
                "VALUES (:name, :date, :is_workout_day, :meal_plan_id) RETURNING id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("name", schedule.getName());
        params.addValue("date", schedule.getDate());
        params.addValue("is_workout_day", schedule.isWorkoutDay());
        params.addValue("meal_plan_id", schedule.getMealPlanId());

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(sql, params, keyHolder);

        int generatedId = Objects.requireNonNull(keyHolder.getKey()).intValue();

        schedule.setId(generatedId);

        return schedule;
    }

    public Schedule updateSchedule(Schedule schedule, Integer scheduleId) {
        String sql = "UPDATE schedule " +
                "SET name = :name, date = :date, is_workout_day = :is_workout_day, meal_plan_id = :meal_plan_id " +
                "WHERE id = :id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", scheduleId);
        params.addValue("name", schedule.getName());
        params.addValue("date", schedule.getDate());
        params.addValue("is_workout_day", schedule.isWorkoutDay());
        params.addValue("meal_plan_id", schedule.getMealPlanId());

        jdbcTemplate.update(sql, params);

        return schedule;
    }

    public Schedule getScheduleByDate(Date date) {
        String sql = "SELECT id, name, date, is_workout_day, meal_plan_id FROM schedule WHERE date = :date";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("date", date);

        return jdbcTemplate.queryForObject(sql, params, scheduleRowMapper);
    }
}