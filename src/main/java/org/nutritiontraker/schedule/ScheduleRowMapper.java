package org.nutritiontraker.schedule;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class ScheduleRowMapper implements RowMapper<Schedule> {

    @Override
    public Schedule mapRow(ResultSet rs, int rowNum) throws SQLException {
        Schedule schedule = new Schedule();
        schedule.setId(rs.getInt("id"));
        schedule.setName(rs.getString("name"));
        schedule.setDate(rs.getDate("date"));
        schedule.setWorkoutDay(rs.getBoolean("is_workout_day"));
        schedule.setMealPlanId(rs.getInt("meal_plan_id"));
        return schedule;
    }
}