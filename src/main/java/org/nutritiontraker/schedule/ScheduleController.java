package org.nutritiontraker.schedule;

import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/schedules")
public class ScheduleController {

    private final ModelMapper modelMapper;
    private final ScheduleService scheduleService;

    public ScheduleController(ModelMapper modelMapper, ScheduleService scheduleService) {
        this.modelMapper = modelMapper;
        this.scheduleService = scheduleService;
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ScheduleDto createSchedule(@RequestBody ScheduleDto scheduleDto) {
        Schedule schedule = modelMapper.map(scheduleDto, Schedule.class);
        Schedule scheduleCreated = scheduleService.createSchedule(schedule);
        return modelMapper.map(scheduleCreated, ScheduleDto.class);
    }

    @PutMapping("/{id}")
    public ScheduleDto updateSchedule(@RequestBody ScheduleDto scheduleDto, @PathVariable Integer id) {
        Schedule schedule = modelMapper.map(scheduleDto, Schedule.class);
        Schedule scheduleUpdated = scheduleService.updateSchedule(schedule, id);
        return modelMapper.map(scheduleUpdated, ScheduleDto.class);
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ScheduleDto getScheduleByDate(@RequestParam Date date) {
        Schedule schedule = scheduleService.getScheduleByDate(date);
        return modelMapper.map(schedule, ScheduleDto.class);
    }
}
