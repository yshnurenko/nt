package org.nutritiontraker.schedule;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.sql.Date;

@Service
public class ScheduleService {

    private final ScheduleRepository scheduleRepository;

    public ScheduleService(ScheduleRepository scheduleRepository, ModelMapper modelMapper) {
        this.scheduleRepository = scheduleRepository;
    }

    public Schedule createSchedule(Schedule schedule) {
        return scheduleRepository.saveSchedule(schedule);
    }

    public Schedule updateSchedule(Schedule schedule, Integer scheduleId) {
        return scheduleRepository.updateSchedule(schedule, scheduleId);
    }

    public Schedule getScheduleByDate(Date date) {
        return scheduleRepository.getScheduleByDate(date);
    }
}
