CREATE TABLE schedule
(
    id             SERIAL PRIMARY KEY,
    name           VARCHAR(20) NOT NULL,
    date           DATE        NOT NULL,
    is_workout_day BOOLEAN     NOT NULL,
    meal_plan_id   INT         NOT NULL REFERENCES meal_plan (id)
);