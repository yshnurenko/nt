CREATE TABLE meal_plan
(
    id          SERIAL PRIMARY KEY,
    name        VARCHAR(20) NOT NULL,
    app_user_id INT         NOT NULL REFERENCES app_user (id)
);