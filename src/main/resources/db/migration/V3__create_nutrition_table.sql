CREATE TABLE nutrition
(
    id             SERIAL PRIMARY KEY,
    meal_type      VARCHAR NOT NULL,
    nutrition_type VARCHAR NOT NULL,
    target         DECIMAL NOT NULL,
    consumed       DECIMAL NOT NULL,
    meal_plan_id   INT     NOT NULL REFERENCES meal_plan (id)
);